<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cast;
use Auth;

class castController extends Controller
{
    public function __construct(){
        $this -> middleware('auth') -> except(['create'], 'update', 'store', 'edit');
    }
    public function create()
    {
        return view('cast.create'); 
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required',
        ]); 

        // DB::table('cast')->insertGetId(
        //     [
        //         'nama' => $request['nama'],
        //         'umur' => $request['umur'],
        //         'bio'  => $request['bio']   
        //     ]
        // );

        //menggunakan model
        // $cast = new Cast;
        // $cast -> nama = $request["nama"];
        // $cast -> umur = $request["umur"];
        // $cast -> bio = $request["bio"];
        // $cast -> save(); // akan menlakukan insert (nama, umur bio) value

        $cast = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
            "user_id" => Auth::id()
        ]);
        
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index',compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.edit', compact('cast'));
     }

     public function update($id, Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]); 
        
        DB::table('cast')->where('id', $id)->update(

            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio'  => $request['bio']
            ]
            );

            return redirect('/cast');
     }

     public function destroy($id){
     
        DB::table('cast')-> where('id','=',$id)->delete();
        return redirect('/cast');
    } 

    
}
