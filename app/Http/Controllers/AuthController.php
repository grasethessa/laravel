<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function two(){
        return view('halaman.register');
    }

    public function three(Request $request){
       // dd($request->all());

       $nama_depan=$request['nama_depan'];
       $nama_belakang=$request['nama_belakang'];
       
       return view('halaman.welcome', compact('nama_depan', 'nama_belakang'));
    }
}
