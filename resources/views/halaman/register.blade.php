@extends('master')
@section('judul')
Halaman Pendataan
@endsection
@section('content')
  <form action = "/submit" method = "post">
    @csrf
    <label for="nama_depan">First Name :</label><br><br>
    <input type = "text" name="nama_depan"><br><br>
    <label for="nama_belakang">Last Name :</label><br><br>
    <input type = "text" name="nama_belakang"> <br><br>

    <label> Gender </label> <br>
    <input type = "radio" name="Gender" checked> Male <br>
    <input type = "radio" name="Gender"> Female <br><br>

    <label> Nationality </label><br><br>
    <select>
        <option value="0"> Indonesia</option>
        <option value="1"> Inggris</option>
        <option value="2"> Jepang</option>
        <option value="3"> India</option>
    </select><br><br>

    <label> Languange Spoken </label><br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English <br>
    <input type="checkbox"> Other <br><br>

    <label for="biodata" > Bio </label><br>
    <textarea cols="30" rows ="8" id="biodata"></textarea><br>
    <input type="submit" value="Sign Up">
@endsection