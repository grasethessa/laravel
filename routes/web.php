<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/data-table', function(){
  return view('halaman.data-table');
});

Route::get('/table', function(){
  return view('halaman.table');
});

Route::get('/', 'HomeController@one');
Route::get('/signup', 'AuthController@two');

Route::post('/submit', 'AuthController@three');

// CRUD Cast
// CREATE
// mengarah ke form create data
// Route::get('cast/create', 'CastController@create');
// menyimpan data ke dataabse
// Route::post('/cast', 'castController@store');

// READ
// menampilkan semua data
// Route::get('/cast', 'castController@index');
// data selengkapnya
// Route::get('/cast/{cast_id}', 'castController@show');

// UPDATE
// mengarah ke form edit data
// Route::get('/cast/{cast_id}/edit', 'castController@edit' );
// update data ke table cast
// Route::put('/cast/{cast_id}', 'castController@update' );

// DELETE
// Route::delete('/cast/{cast_id}', 'castController@destroy' );

// menyederhanakannya dengan controller dan route resource
// Route::resource('casting', 'castingController');

Route::resource('cast', 'castController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
